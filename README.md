# Sample App 

## Full-Functional, 100% Tested Rails Application

The idea of this twitter-clone application belongs to Michael Hartl, the creator of the famous [Ruby on Rails Tutorial](http://ruby.railstutorial.org/ruby-on-rails-tutorial-book). 

It's quite simple: there are *Users* who can login and write *Microposts*. The can follow each other to read the Micropost stream.