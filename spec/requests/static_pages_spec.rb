require 'spec_helper'

describe "StaticPages" do

  it "should have the right links on the layout" do 
      visit root_path
      click_link "About"
      page.should have_selector('title', text: full_title('About Us'))
      click_link "Help"
      page.should have_selector('title', text: full_title('Help'))
      click_link "Contact"
      page.should have_selector('title', text: full_title('Contact'))
      click_link "Home"
      page.should have_selector('title', text: full_title(''))
      click_link "Sign up now!"
      page.should have_selector('title', text: full_title('Sign up'))
  end

  # Page is the subject of the test suite
  subject { page }

  # Eliminate repetition by using shared_examples
  shared_examples_for "all static pages" do
    it { should have_selector('h1', text: heading) }
    it { should have_selector('title', text: full_title(page_title)) }
  end

  describe "Home page" do

    before { visit root_path }
    # Defining some local vars to be used in the function
    let(:heading)     { 'Sample App' }
    let(:page_title)  { '' }

    # 'it' meaning page in this context - since we've defined it as subject
    # using function defined by 'shared_examples_for' here
    it_should_behave_like "all static pages"

    it { should_not have_selector('title', text: ' | Home') }

  end # Home Page

  describe "Help page" do

    before { visit help_path }
    let(:heading) { 'Help' }
    let(:page_title) { ' | Help' }

  end # About Page

  describe "About page" do

    before { visit about_path }
    let(:heading) { 'About' }
    let(:page_title) { ' | About Us' }

  end # About Page

  describe "Contact page" do

    before { visit contact_path }
    let(:heading) { 'Contact' }
    let(:page_title) { ' | Contact' }

  end # Contact Page

end # StaticPages