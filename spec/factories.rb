FactoryGirl.define do
	factory :user do
		name					"Andrey Esaulov"
		email					"esaulov.andrey@gmail.com"
		password 				"foobar"
		password_confirmation 	"foobar"
	end
end